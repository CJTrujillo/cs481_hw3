
//
// CJ Trujillo 9/24/20
// CS 481 HW2
// Layout Homework
// Displays image, icons and text for pets
//  when paw is pressed the pet counter increments
//

import 'package:flutter/material.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: PettingWidget(),
    );
  }
}

class PettingWidget extends StatefulWidget {
  @override
  _PettingWidgetState createState() => _PettingWidgetState();
}

class _PettingWidgetState extends State<PettingWidget> {
  int _nutellaCount = 483;
  int _biscoffCount = 402;
  int _buffyCount = 53;
  // This widget is the root of your application.
  @override

  Widget build(BuildContext context) {

    //Creates title sections for pets
    Widget nutellaTitle= Container(
        padding: const EdgeInsets.all(32),
        child:Row(
          children: [
            Expanded(
              child: Column(    //column = children stacked
                crossAxisAlignment: CrossAxisAlignment.start,   //.start = top justified
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text('This is my Dog Nutella', style: TextStyle(fontWeight: FontWeight.bold,), //textstyle
                    ),
                  ),
                  Text('She\'s so cute, right?', style: TextStyle(color: Colors.grey[500],), //textstyle
                  ),
                ], //children
              ),
            ),
            IconButton(
              icon: Icon(Icons.pets),
              color: Colors.red[500],
              onPressed: _petNutella,
            ),
            Text(_nutellaCount.toString()),
          ], //children
        )
    );
    Widget biscoffTitle= Container(
        padding: const EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
              child: Column(    //column = children stacked
                crossAxisAlignment: CrossAxisAlignment.start,   //.start = top justified
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text('This is my Dog Biscoff', style: TextStyle(fontWeight: FontWeight.bold,), //textstyle
                    ), //text
                  ), //container
                  Text('He\'s so handsome, right?', style: TextStyle(color: Colors.grey[500],), //textstyle
                  ), //text
                ], //children
              ), //column
            ), //expanded
            IconButton(
              icon: Icon(Icons.pets),
              color: Colors.red[500],
              onPressed: _petBiscoff,
            ),
            Text(_biscoffCount.toString()),
          ], //children
        )
    ); //container
    Widget buffyTitle= Container(
        padding: const EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
              child: Column(    //column = children stacked
                crossAxisAlignment: CrossAxisAlignment.start,   //.start = top justified
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text('This is my Cat, Buffy', style: TextStyle(fontWeight: FontWeight.bold,), //textstyle
                    ),
                  ),
                  Text('Se\'s so elegant, right?', style: TextStyle(color: Colors.grey[500],), //textstyle
                  ),
                ], //children
              ),
            ),
            IconButton(
              icon: Icon(Icons.pets),
              color: Colors.red[500],
              onPressed: _petBuffy,
            ),
            Text(_buffyCount.toString()),
          ], //children
        )
    );

    Color color = Theme.of(context).primaryColor;

    //Creates button section for each pet
    Widget nutellaButton = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.cake, '12/23/2011'),
          _buildButtonColumn(color, Icons.sentiment_dissatisfied, 'Cranky'),
          _buildButtonColumn(color, Icons.restaurant, 'Beef Esophagus'),
        ],
      ),
    ); //Container
    Widget biscoffButton = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.cake, '7/25/2014'),
          _buildButtonColumn(color, Icons.sentiment_very_satisfied, 'Playful'),
          _buildButtonColumn(color, Icons.restaurant, 'Dentastix'),
        ],
      ),
    ); //Container
    Widget buffyButton = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.cake, '4/19/2006'),
          _buildButtonColumn(color, Icons.sentiment_dissatisfied, 'Cranky'),
          _buildButtonColumn(color, Icons.restaurant, 'Tuna Paste'),
        ],
      ),
    ); //Container

    //Writes text section for each pet
    Widget nutellaText = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This is my first dog Nutella. We named her that because we'
            ' love both the spread and the dog. We\'ve had her since'
            ' 2012. She tap dances when someone she likes walks into '
            'the room. She dislikes most people.',
        softWrap: true,

      ),
    );
    Widget biscoffText = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This is my second dog Biscoff. We named him that because we'
            ' wanted both dogs to be a food spread. We got him when Nutella\'s'
            ' sister had puppies. He is Nutella\'s nephew. He likes to '
            'act tough by barking at any leaf or squirrel that passes. '
            'He\'ll cry if it doesn\'t leave though.',
        softWrap: true,
      ),
    );
    Widget buffyText = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This is my third pet Buffy. We named her that because'
            ' she hunts like the Vampire Slayer. We got her after her previous'
            ' owner passed away. She was very shy and is still getting used to us. '
            'She wants constant attention but only some pets.',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/Nutella.jpg',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              nutellaTitle,
              nutellaButton,
              nutellaText,
              Image.asset(
                'images/Biscoff.jpg',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              biscoffTitle,
              biscoffButton,
              biscoffText,
              Image.asset(
                'images/Buffy.jpg',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              buffyTitle,
              buffyButton,
              buffyText,
            ]
        ),
      ),
    );
  }


//Changes the count on pets when button is pressed
  void _petNutella(){
    setState(() {
      _nutellaCount++;
    });
  }

  void _petBiscoff(){
    setState(() {
      _biscoffCount++;
    });
  }

  void _petBuffy(){
    setState(() {
      _buffyCount++;
    });
  }

  //builds button bar based on the pet inputs
  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color:color),
        Container(
          margin: const EdgeInsets.only(top:8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ], //children
    );
  }
}